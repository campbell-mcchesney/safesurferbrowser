﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace safesurferbrowser
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<WebHistory> hisPicker = new ObservableCollection<WebHistory>();
        public MainPage()
        {
            InitializeComponent();            
            WebNavigate(App.admin.home);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UserName.Text = "Hello "+App.user[App.userIndex].userName; //displays the current user in the title bar
            FavoritesPickerPopulate(); 
            HistoryPickerPopulate();
        }

        public void FavoritesPickerPopulate()
        {            
            FavoritesPicker.Items.Clear();
            if (App.favorites != null)
            {
                foreach (var item in App.favorites)
                {
                    if(item.userName == App.user[App.userIndex].userName) //this only adds the favourites for the current user
                    {
                        FavoritesPicker.Items.Add(item.siteName);
                    }
                    
                }
            }
        }

        public void HistoryPickerPopulate()
        {
            ; //a new local observable collection for the picker
            HistoryPicker.Items.Clear();
            if (App.history != null) //only adds to the picker if the collection has items
            {
                bool exists; //created to check if the item already exists or not
                for(int i = 0; i < App.history.Count; i++) //iterates through every record in the history list
                {
                    exists = false; //each iteration begins assuming the item does not already exist
                    for(int j = i+1; j < App.history.Count; j++) //this loop checks every record after the current record
                    {
                        if(App.history[i].siteName == App.history[j].siteName) //if there is a match then it is flagged as true
                        {
                            exists = true;
                        }                      
                    }
                    if (App.history[i].userName == App.user[App.userIndex].userName && App.history[i].flagged == 0 && exists == false) //if the item belongs to the current user, has not been flagged by the admin and does not alreadt exists then it is added
                    {
                        hisPicker.Add(new WebHistory()); //a new object is created
                        int count; // this next section of code makes sure that the index is always >= 0
                        if (hisPicker.Count >= 1)
                        {
                            count = hisPicker.Count - 1;
                        }
                        else
                        {
                            count = 0;
                        }
                        hisPicker[count].userName = App.history[i].userName; //this block of code updates the added object with data
                        hisPicker[count].siteName = App.history[i].siteName;
                        hisPicker[count].siteUrlHis = App.history[i].siteUrlHis;
                        hisPicker[count].date = App.history[i].date;
                        hisPicker[count].flagged = App.history[i].flagged;
                        HistoryPicker.Items.Add(hisPicker[count].siteName); //Adds the sitename from the object to the picker
                    }
                }
            }
        }

        public void WebNavigate(string _url) //this method checks the url and adds the http protocol if needed
        {
            if (_url.Contains("http://"))
            {
                MainWebView.Source = _url;
            }

            else if (_url.Contains("https://"))
            {
                MainWebView.Source = _url;
            }

            else if (_url.Contains("http://www."))
            {
                MainWebView.Source = _url;
            }

            else if (_url.Contains("https://www."))
            {
                MainWebView.Source = _url;
            }

            else
            {
                string altUrl = "http://www." + _url;
                MainWebView.Source = altUrl;
            }

        }
       

        private void Go_Clicked(object sender, EventArgs e)
        {
            WebNavigate(UrlEntryBox.Text);
        }

        private void BackBtn_Clicked(object sender, EventArgs e)
        {
            MainWebView.GoBack();
        }

        private void FwdBtn_Clicked(object sender, EventArgs e)
        {
            MainWebView.GoForward();
        }

        private async void MainPageLogout_Clicked(object sender, EventArgs e)
        {
            var confirmed = await DisplayAlert("Confirm", "Are you sure you want to log out?", "Yes", "No");
            if (confirmed)
            {
                await Navigation.PushAsync(new LoginPage());
            }
            else
            {
                
            }
            
        }

        private void MainWebView_Navigated(object sender, WebNavigatedEventArgs e)
        {           
            App.currentUrl = e.Url; //this block of code below formats the url for use as a favorite or history picker item label
            Uri uri = new Uri(App.currentUrl, UriKind.Absolute);            
            App.favName = uri.GetLeftPart(UriPartial.Authority);
            string host = uri.Host;
            string path = uri.AbsolutePath;
            
            UrlEntryBox.Text = App.currentUrl; //displays the current url in the url entry box
            App.history.Add(new WebHistory()); //creates a new object in the history list
            int count;
            if(App.history.Count > 1)
            {
                count = App.history.Count - 1;
            }
            else
            {
                count = 0;
            }
            App.history[count].userName = App.user[App.userIndex].userName;
            App.hisName = host + path;
            App.history[count].siteName = App.hisName;
            App.history[count].siteUrlHis = App.currentUrl;
            App.history[count].date = DateTime.Now.Date;
            App.history[count].flagged = 0;
            App.conn.Insert(App.history[count]);
            HistoryPickerPopulate();
        }

        private void MainWebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            bool action = false;
            if (App.admin.listOnOff == 1) //checks to see if Block/Allow Lists have been enabled
            {
                if (App.admin.listSelected == 0) //Means the Allow List has been enabled
                {
                    foreach (var item in App.allowList)
                    {
                        if (e.Url.Contains(item.urlAllowItem))
                        {
                            action = true; //if the URL being navigated to is in the Allow List then action is set to true
                        }
                        else
                        {
                            action = false;
                        }
                    }
                    if (action == false)
                    {
                        e.Cancel = true; //if action is set to false and the site is not allowed then the navigation is cancelled
                        DisplayAlert("Navigation Canceled", "The Admin has blocked this page", "OK");
                    }
                }

                else //Means the Block List is enabled
                {
                    foreach (var item in App.blockList)
                    {
                        if (item.urlBlockItem != null)
                        {
                            if (e.Url.Contains(item.urlBlockItem))
                            {
                                action = true; //If the URL is in the Block List then action is set to true
                            }
                            else
                            {
                                action = false;
                            }
                        }
                    }
                    if (action == true) 
                    {
                        e.Cancel = true; //if action is set to true then the URL is in the Block and navigation is cancelled
                        DisplayAlert("Navigation Canceled", "The Admin has blocked this page", "OK");
                    }
                }
            }
            App.currentUrl = e.Url; //This block of code sets the Favourite Name so that it cann be added to Favourites if clicked
            Uri uri = new Uri(App.currentUrl, UriKind.Absolute);
            App.favName = uri.GetLeftPart(UriPartial.Authority);
            UrlEntryBox.Text = App.currentUrl;            
        }



        private void Home_Clicked(object sender, EventArgs e)
        {
            WebNavigate(App.admin.home); //Navigates the page to the admin defined homepage
        }

        private void AddFav_Clicked(object sender, EventArgs e)
        {
            int count;
            App.favorites.Add(new FavoritesData());
            if(App.favorites.Count > 1)
            {
                count = App.favorites.Count - 1;
            }
            else
            {
                count = 0;
            }
            //This block of code sets the Favourite object values to be the same as the current user and page url
            App.favorites[count].userName = App.user[App.userIndex].userName;
            App.favorites[count].siteName = App.favName;
            App.favorites[count].siteUrl = App.currentUrl;
            App.conn.Insert(App.favorites[count]);
            FavoritesPickerPopulate();
        }

        private void FavouritesPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(FavoritesPicker.SelectedIndex != -1)
            {
                App.favindex = FavoritesPicker.SelectedIndex;
                WebNavigate(App.favorites[App.favindex].siteUrl);
                FavoritesPicker.SelectedIndex = -1;
            }
                      
        }        

        private void HistoryPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(HistoryPicker.SelectedIndex != -1)
            {
                App.hisIndex = HistoryPicker.SelectedIndex;
                WebNavigate(hisPicker[App.hisIndex].siteUrlHis);
                HistoryPicker.SelectedIndex = -1;
            }

        }

        private void SettingsPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            //This is a place holder method for future development
        }
    }
}
