﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Plugin.MtSql;
using SQLite;



namespace safesurferbrowser
{
    public partial class App : Application
    {
        //All of these variables and collections are accessible across the entire App
        public static int userIndex;
        public static string currentUrl;
        public static string favName;
        public static int favindex;
        public static string hisName;
        public static int hisIndex;

        public static List<Person> user = new List<Person>();
        public static List<FavoritesData> favorites = new List<FavoritesData>();
        public static List<WebHistory> history = new List<WebHistory>();
        public static List<GlobalBlockList> blockList = new List<GlobalBlockList>();
        public static List<GlobalAllowList> allowList = new List<GlobalAllowList>();
        public static AdminSettingsData admin = new AdminSettingsData();
        public static SQLiteConnection conn = CrossMtSql.Current.GetConnection("AppData.db3");       
        

        public App()
        {
            //Tables will be created if they do not already exists, otherwise the create commands are ignored if the tables exist
            conn.CreateTable<AdminSettingsData>();
            conn.CreateTable<Person>();
            conn.CreateTable<FavoritesData>();
            conn.CreateTable<WebHistory>();
            conn.CreateTable<GlobalAllowList>();
            conn.CreateTable<GlobalBlockList>();
            //Each time the app starts the entire database is loaded into the collections and objects
            user = conn.Query<Person>("Select * From Person");
            favorites = conn.Query<FavoritesData>("Select * From FavoritesData");
            history = conn.Query<WebHistory>("Select * From WebHistory");
            blockList = conn.Query<GlobalBlockList>("Select * from GlobalBlockList");
            allowList = conn.Query<GlobalAllowList>("Select * from GlobalAllowList");            

            try
            {
                admin = conn.Get<AdminSettingsData>(0); //This will try and load the admin data into the object from the database
            }
            catch //If the admin table is empty because this is the first time the app has been run then blank data will be added
            {
                admin.id = 0;
                admin.password = "";
                admin.email = "";
                admin.home = "";
                admin.listSelected = 0;
                admin.listOnOff = 0;
                conn.Insert(admin);
            }
            
            InitializeComponent();
            MainPage = new NavigationPage(new LoginPage()); //The Login Page is the landing page of the app.
        }       
       
        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
