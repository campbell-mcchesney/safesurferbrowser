﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace safesurferbrowser
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {            
            InitializeComponent();           
            
            if (App.admin.password == "") //Switches the visible grid depending what action is current
            {
                LoginGrid.IsVisible = false;
                AdminCreate.IsVisible = true;
            }

            else
            {
                LoginGrid.IsVisible = true;
                AdminCreate.IsVisible = false;
            }
            
        }

        protected override void OnAppearing() //sets up the user picker
        {
            base.OnAppearing();           
            UserListView.Title = "Select User";
            UserListView.Items.Clear();
            if (App.user != null)
            {                
                foreach (var item in App.user)
                {
                    UserListView.Items.Add(item.userName);
                }
            }

        }

        private void UserListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            App.userIndex = UserListView.SelectedIndex;            
        }

        private void AddPass_Clicked(object sender, EventArgs e)
        {
            App.admin.password = NewAdminPass.Text; //updates the new password in the admin object         
            App.conn.Update(App.admin); //updates the updated object in the database
            LoginGrid.IsVisible = true; //these next two lines switch the visible grids
            AdminCreate.IsVisible = false;
        }

        private async void LoginBtn_Clicked(object sender, EventArgs e)
        {
            if (App.userIndex != -1) //only actions the click if a user is selected
            {
                if (LoginEntry.Text == App.user[App.userIndex].userPassword) //checks the entered password against the stored password
                {
                    await Navigation.PushAsync(new MainPage());
                }
            }
        }

        private async void AdminLoginBtn_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AdminPage());
        }
    }
}