﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace safesurferbrowser
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdminPage : ContentPage
    {       
        public int userIndexLocal;
        public int BAtoggleState;
        public int BAIndex;
        ObservableCollection<GlobalAllowList> localAllowList = new ObservableCollection<GlobalAllowList>(App.allowList);
        ObservableCollection<GlobalBlockList> localBlockList = new ObservableCollection<GlobalBlockList>(App.blockList);

        public AdminPage()
        {            
            InitializeComponent();
            BindingContext = this;
            AllowList.BindingContext = localAllowList;
            AllowList.ItemsSource = localAllowList;
            BlockList.BindingContext = localBlockList;
            BlockList.ItemsSource = localBlockList;   
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            AdminMainLayout.IsVisible = false; //These two lines hide the main admin grid
            LogOutBtn.IsVisible = false;      //until the Admin has logged in.
            AdminLoginLayout.IsVisible = true; //This shows the login grid.
            if(App.admin.listOnOff == 0) //This sets the state of the toggle that turns all Block/Allow lists on and off
            {
                ListOnOff.IsToggled = false;
            }
            else
            {
                ListOnOff.IsToggled = true;
            }
            if(App.admin.listSelected == 0) //This sets the state of the toggle that switches between Block/Allow Lists.
            {
                AllowList.IsVisible = true;
                AllowList.IsEnabled = true;
                BlockList.IsVisible = false;
                BlockList.IsEnabled = false;
                BAToggle.IsToggled = false;
                BAtoggleState = 0;
            }
            else
            {
                AllowList.IsVisible = false;
                AllowList.IsEnabled = false;
                BlockList.IsVisible = true;
                BlockList.IsEnabled = true;
                BAToggle.IsToggled = true;
                BAtoggleState = 1;
            }
            
            UserPickerPopulate(); //This calls the method that populates the UserPicker
            AdminInfoPopulate();  //This calls the method that populates the AdminInfo Text Entries        
        }

        

        public void UserPickerPopulate()
        {            
            UserPicker.Items.Clear();
            if (App.user != null)
            {                
                foreach (var item in App.user)
                {
                    UserPicker.Items.Add(item.userName);
                }
            }
        }

        public void AdminInfoPopulate()
        {            
            AdminPassEntry.Text = App.admin.password;
            AdminEmailEntry.Text = App.admin.email;
            HomePageEntry.Text = App.admin.home;            
        }

        private void UserPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            userIndexLocal = UserPicker.SelectedIndex; //sets the variable userLocalIndex to the picker item index
            try
            {
                UserNameEntry.Text = App.user[userIndexLocal].userName;
            }
            catch
            {
                UserNameEntry.Text = "";
            }
            try
            {
                UserPassEntry.Text = App.user[userIndexLocal].userPassword;
            }
            catch
            {
                UserPassEntry.Text = "";
            }
            try
            {
                if (App.user[userIndexLocal].userEnabled == 1) //Converts the value stored in the collection from and int to a boolean
                {
                    UserToggle.IsToggled = true;
                }
                else
                {
                    UserToggle.IsToggled = false;
                }
            }
            catch
            {
                UserToggle.IsToggled = false;
            }
            
        }

        private void Collection_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            
        }

        private void UserToggle_Toggled(object sender, ToggledEventArgs e) //Sets the toggle value to an int
        {
            try
            {
                if (UserToggle.IsToggled)
                {
                    App.user[userIndexLocal].userEnabled = 1;
                }
                else
                {
                    App.user[userIndexLocal].userEnabled = 0;
                }
            }
            catch
            {

            }
        }

        private void AdminPassEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            App.admin.password = AdminPassEntry.Text;
            App.conn.Update(App.admin);
        }

        private void AdminEmailEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            App.admin.email = AdminEmailEntry.Text;
            App.conn.Update(App.admin);
        }

        private void HomePageEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            App.admin.home = HomePageEntry.Text;
            App.conn.Update(App.admin); 
        }

        private void UpdateButton_Clicked(object sender, EventArgs e)
        {
            App.user[userIndexLocal].userName = UserNameEntry.Text;            
            App.user[userIndexLocal].userPassword = UserPassEntry.Text;
            if(UserToggle.IsToggled == true) //Converts the toggle state to an int
            {
                App.user[userIndexLocal].userEnabled = 1;
            }
            else
            {
                App.user[userIndexLocal].userEnabled = 0;
            }
            App.conn.Update(App.user[userIndexLocal]); //Updates the user data in the database
        }

        private void DeleteButton_Clicked(object sender, EventArgs e)
        {
            int userIndex = userIndexLocal; //This needs to be a copy or the program crashes... Don't know why.
            userIndexLocal = 0; 
            App.conn.Query<Person>("Delete from Person where userName = @userName", App.user[userIndex].userName); //Database record must be removed first
            App.user.RemoveAt(userIndex); //Record then remmoved from the collection         
            UserPicker.SelectedIndex = -1; // Picker index set to -1 so no user is displayed
            UserPickerPopulate();           
            UserNameEntry.Text = "";
            UserPassEntry.Text = "";
            UserToggle.IsToggled = false;
        }

        public void CreateButton_Clicked(object sender, EventArgs e)
        {
            
            if (CreateBtn.Text == "Create User") //Button label is used to determine what action to take 
            {
                UserPicker.IsVisible = false; //This block of code will be executed to set up the entry environment
                UserNameEntry.Text = "";
                UserPassEntry.Text = "";
                UserToggle.IsToggled = true;
                UpdateBtn.IsEnabled = false;
                DeleteBtn.IsEnabled = false;
                CancelEntry.IsVisible = true;
                CreateBtn.Text = "Add User"; //Button label is changed so that the next action can be executed
            }
            else
            {
                App.user.Add(new Person()); //New trecord is created in the collection
                int count; //A new local variable is created to set the user index
                if (App.user.Count >= 1)
                {
                    count = App.user.Count - 1;
                }
                else
                {
                    count = 0;
                }
                App.user[count].userName = UserNameEntry.Text;
                App.user[count].userPassword = UserPassEntry.Text;
                App.user[count].userPic = "";
                App.user[count].userEnabled = 1;
                App.conn.Insert(App.user[count]); //The new user that has been created and data added is inserted into the database
                UserPickerPopulate(); //The userpicker is repopulated with the new user
                UserPicker.IsVisible = true; //The rest of the code in this block returns the elements to a state needed for existing user manipulation
                UserNameEntry.Text = "";
                UserPassEntry.Text = "";
                UserToggle.IsToggled = false;
                UpdateBtn.IsEnabled = true;
                DeleteBtn.IsEnabled = true;
                CancelEntry.IsVisible = false;
                CreateBtn.Text = "Create User";
            }
        }

        private void CancelEntry_Clicked(object sender, EventArgs e) //If the admin decides not to add a user this code will cancel the action
        {
            UserPicker.IsVisible = true;
            UserNameEntry.Text = "";
            UserPassEntry.Text = "";
            UserToggle.IsToggled = false;
            UpdateBtn.IsEnabled = true;
            DeleteBtn.IsEnabled = true;
            CancelEntry.IsVisible = false;
            CreateBtn.Text = "Create User";
        }

        private void AdminLogin_Clicked(object sender, EventArgs e)
        {            
            if (AdminLoginEntry.Text == App.admin.password) //This checks the entered password against the stored password
            {
                AdminMainLayout.IsVisible = true; //If the password matches then the login grid disappears and the main grids appear
                LogOutBtn.IsVisible = true;
                AdminLoginLayout.IsVisible = false;
            }
        }

        private async void LogOutButton_Clicked(object sender, EventArgs e)
        {
            var confirmed = await DisplayAlert("Confirm", "Are you sure you want to log out?", "Yes", "No");
            if (confirmed)
            {
                await Navigation.PushAsync(new LoginPage());
            }
            else
            {

            }
        }

       

        private async void CancelButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginPage());
        }

        private void BAToggle_Toggled(object sender, ToggledEventArgs e)
        {
           if(BAToggle.IsToggled == true) //This toggles bewteen Block and Allow lists
            {
                AllowList.IsVisible = false;
                BlockList.IsVisible = true;
                BAtoggleState = 1;
                App.admin.listSelected = 1;
                App.conn.Update(App.admin);
            }
            else
            {
                AllowList.IsVisible = true;
                BlockList.IsVisible = false;
                BAtoggleState = 0;
                App.admin.listSelected = 0;
                App.conn.Update(App.admin);
            }
        }

        

        private void AddBAList_Clicked(object sender, EventArgs e)
        {
           if(BAtoggleState == 0)
            {
                int lastRec; //This int is set to the index of the last record in the collection
                localAllowList.Add(new GlobalAllowList()); //This is the local list that is an observable collection
                App.allowList.Add(new GlobalAllowList()); //This is the master list (Type List<>) in the App.cs (An observable collection cannot interact with the database)
                if(localAllowList.Count > 1)
                {
                    lastRec = localAllowList.Count - 1;
                }
                else
                {
                    lastRec = 0;
                }                
                localAllowList[lastRec].urlAllowItem = BAListEntry.Text; //Adds to the local observable collection                           
                App.allowList[lastRec].urlAllowItem = BAListEntry.Text; //Adds to the master List in App.cs
                App.conn.Insert(App.allowList[lastRec]); //Adds the record in master List to database
                localAllowList.CollectionChanged += Collection_Changed;               
                BAListEntry.Text = "";
            }

            else
            {
                int lastRec; //All of this code is the same as above but for the BlockList
                localBlockList.Add(new GlobalBlockList());
                App.blockList.Add(new GlobalBlockList());
                if(localBlockList.Count > 1)
                {
                    lastRec = localBlockList.Count - 1;
                }
                else
                {
                    lastRec = 0;
                }
                
                localBlockList[lastRec].urlBlockItem = BAListEntry.Text;
                App.blockList[lastRec].urlBlockItem = BAListEntry.Text;
                App.conn.Insert(App.blockList[lastRec]);
                localBlockList.CollectionChanged += Collection_Changed;                
                BAListEntry.Text = "";
            }
        }

        private void DeleteBA_Clicked(object sender, EventArgs e)
        {
            if(BAtoggleState == 0) //Sets the conditions for either the Block or Allow list to be modified
            {
                App.conn.Query<GlobalAllowList>("DELETE FROM GlobalAllowList WHERE urlAllowItem = @url", localAllowList[BAIndex].urlAllowItem); //Deletes the record from the database first
                localAllowList.RemoveAt(BAIndex); //Removes from the local observable collection
                App.allowList.RemoveAt(BAIndex); //Removes from the master list in App.cs
                localAllowList.CollectionChanged += Collection_Changed;
            }

            else 
            {
                App.conn.Query<GlobalBlockList>("DELETE FROM GlobalBlockList WHERE urlBlockItem = @url", localBlockList[BAIndex].urlBlockItem);
                localBlockList.RemoveAt(BAIndex);
                App.blockList.RemoveAt(BAIndex);
                localBlockList.CollectionChanged += Collection_Changed;
            }
        }

        private void AllowList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            BAIndex = App.allowList.IndexOf(e.SelectedItem as GlobalAllowList); //Sets the index for the collection as the same as the listview index
            
            Temp1.Text = BAIndex.ToString();
        }

        private void BlockList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            BAIndex = App.blockList.IndexOf(e.SelectedItem as GlobalBlockList); //Sets the index for the collection as the same as the listview index

            Temp1.Text = BAIndex.ToString();
        }

        private void ListOnOff_Toggled(object sender, ToggledEventArgs e) //This is the on/off switch for whether Block/Allow lists are used or not
        {
            if (ListOnOff.IsToggled == false)
            {
                App.admin.listOnOff = 0;
                App.conn.Update(App.admin); //Updates the value in the database
            }
            else
            {
                App.admin.listOnOff = 1;
                App.conn.Update(App.admin); //Updates the value in the database
            }
        }
    }
}