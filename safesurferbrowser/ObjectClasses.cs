﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using Plugin.MtSql;

namespace safesurferbrowser
{
    //These classes are the models for the objects
    //They have been formatted to be used as tables for the SQL database as well
    public class AdminSettingsData
    {
        [PrimaryKey]
        public int id { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string home { get; set; } 
        public int listSelected { get; set; }
        public int listOnOff { get; set; }
    }    

    public class User
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string userName { get; set; }
    }
    public class Person : User
    {   
        public string userPassword { get; set; }
        public string userPic { get; set; }
        public int userEnabled { get; set; }
        
    }

    public class FavoritesData : User
    {
        public string siteName { get; set; }
        public string siteUrl { get; set; }               
    }

    public class WebHistory : User
    {
        public string siteName { get; set; }
        public string siteUrlHis { get; set; }
        public DateTime date { get; set; }
        public int flagged { get; set; }
        
    }

    public class GlobalBlockList
    {
        public string urlBlockItem { get; set; }        
    }    

    public class GlobalAllowList
    {
        public string urlAllowItem { get; set; }       
    }

   
    public class ProfilePics
    {
        public Uri profPic { get; set; }        
    }
}
